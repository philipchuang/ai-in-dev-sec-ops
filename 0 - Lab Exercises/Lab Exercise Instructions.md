## Theme

The goal of this workshop is to give you a look into some of the features the GitLab AI team is developing, not just Code Suggestions. We've put a big emphasis on helping developers throughout the entire SDLC and not _just_ coding tasks.

**PLEASE NOTE:** Many of these features are in the [experimental phase](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment), which means they may be prone to outages as the dev team is actively working on enhancing them. If this occurs and you didn't see a feature demo live that you hopped to see, please reach out to the account team and we will provide recordings & additional info on when it may be available.

## Key Tasks to Complete


* [ ] **Step 1: Pipeline Kickoff & GitLab Chat**
* ![gitlab_chat.svg](./gitlab_chat.svg)
  * Use the sidebar to click through **Build \> Pipelines** and click **Run pipeline** in the top right.
  * Make sure the **_main_** branch is selected and click **Run pipeline**. We will let this pipeline run and come back later in the workshop.
  * _What if we hadn't known where to access the pipelines from or how to open an MR?_ You can now use the new AI chat feature called [GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html). Go ahead and click the **? Help** button in the bottom left then select **GitLab Duo Chat**
  * You can ask GitLab Duo any related GitLab questions such as: "**Where can I find my running pipelines?**", to which the chat will respond with the path to the pipelines page. Other examples of questions you can ask are:
    * "**What is a fork?**"
    * "**Write a tic tac toe game in Javascript"**
    * "**Generate a summary for the issue identified via this link: <link to your issue>**"
  * While the pipeline runs, take some time to go through [the current AI/ML features GitLab offers](https://docs.gitlab.com/ee/user/ai_features.html)

    > Please note that asking for code suggestions can take a few minutes so it is best to test out post workshop. Additionally, keep in mind that you will have a short trial period post workshop with access to this group that has all of the features enabled, so feel free to try any out that we do not directly cover during the workshop.

  ---
* [ ] **Step 2: Security Results & AI**
* ![explain_this_vuln.svg](./explain_this_vuln.svg) 
  * Next use the left hand navigation menu to click through **Build \> Pipelines** and ensure that the most recent pipeline we kicked off is complete. Select the most recent pipeline that completed (unique number preceded by #).
  * Spend some time taking a look at all of the information provided to you, exploring the job results, vulnerability tab, and other informative tabs.
  * We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability report** to view the full report
  * Once in the **_Vulnerability Report_** we first can click into any of the vulnerabilities present.
  * Next look for the **Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection')** or **_CWE-89 in db.py_** vulnerability and click into one of them. We can see that this vulnerability was triggered by a SQL injection risk. Scroll down and click the **Explain vulnerability** button for an explanation on what a SQL injection risk is and why our application is vulnerable.
  
  * At the end of the report check out the **_Fixed_** code section and we can see that if we paramaterize the query by replacing  `cur.execute(query)`\_ with `cur.execute(query, (user_id,))` it will prevent the possibility of sql injection. We will use this knowledge later in the workshop.

Note: there may be other valid solutions that are suggested by the AI.

  ---
* [ ] **Step 3: Explain This Code**
* ![explain_this_code.svg](./explain_this_code.svg)
  * What if we wanted more context about the specific function above before we went and made a code change? From within the **Vulnerability Report**, click on the **_File_** link under **Location** (ie: _notes/db.py:111_).
  * Once within the db.py file locate the line the sql injection vulnerability was occurring on and highlight the entire function.
  * You should then see a small _question mark_ (?) (_What does the selected code mean?_ ["**Explain this code**"]) to the left of the code, click it.
  * On the right hand side there will now be a small pop up to explain what your highlighted code does in natural language. Try highlighting other code sections as well.
  
  * At this point we should be fully aware of why and how our SQL injection vulnerability is occurring. In the next section we will use Code Suggestions to fix it

    > [Docs for GitLab application security](https://docs.gitlab.com/ee/user/application_security/)

---

* [ ] **Step 4: Coding with [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)**
* ![code_suggestions_svg.svg](./code_suggestions_svg.svg)
  * Now that we have more context around the SQL injection vulnerability lets go try to fix it and do some development with **GitLab Code Suggestions**.
  * Before we make any code changes we will want to create a merge request to track our work. Use the left hand navigation to click through **Code \> Branches**.
  * We then want to click **New Branch**, name it **_ai-test_**, then click **Create branch**. On the resulting page click **Create merge request**.
  * On the resulting page uncheck **Mark as draft**, leave all other settings as is and scroll to the bottom then click **Create merge request**.
  * Next click **Code** in the top right, then click **Open in Web IDE**.
  * Once in the IDE navigate through **notes/db.py** and take a look at the function on line 102. We can see that it is clearly one of the functions that was listed as vulnerable to SQL Injections.
  
  * Specifically on _line 111_ lets use what we learned using **Explain This Vulnerability** to change the line of code to this:

  ```plaintext
       cur.execute(query, (id,))
  ```

  > Please note that the suggested change my have been different depending on which sql vulnerability you looked at
  * Now lets actually use **Code Suggestions** to add a whole new class. We want to add a calculator class to this application so that we can enable calculations in the notes.
  * First right click the **notes** folder then click **new file**. Name this new file **_calc.py_**.
  
  * We then want to add the prompt below to let **Code Suggestions** know what we are trying to write:

  ```plaintext
   # define a calculator class that other functions can call
  ```
  * Press enter after the prompt and then wait for the suggestion to generate. As you are given suggestions, hit the TAB key to accept them. If it ever gets stuck try hitting the space bar or enter.
  * Code suggestions will write a very in depth calculator function and eventually will loop but feel free to stop it after 5 methods.
  * Code Suggestions doesn't just work for python files and supports multiple languages per project. Navigate into the **ai-sandbox/** folder for a list of currently up to date projects.
  * Choose one of the projects and test out code suggestions to write a hello world example or something more advanced.
  * Now we want to commit this code. Go ahead and click the **Source Control** button on the left hand side. Leave the commit message empty and click on the **_Commit to 'AI-test'_.**
 
  * On the bottom right corner, you will see a popup appear; click the **Goto MR** button.

  ---
* [ ] **Step 5: AI in the Merge Request** 
* ![summarize_mr.svg](./summarize_mr.svg)
  * Now that we are back in our MR we should see that our code changes have kicked off another run of our pipeline. We have made a number of changes, so lets use the AI to generate a commit message.
  
  * After the pipeline has run and is _ready to merge_ (you should see a green checkmark followed by **Ready to merge!**; might require page refresh), select the **Edit commit message** checkbox.
  * You will see the **Create AI-generated commit message** button appear below so go ahead and click it. There may be a delay as you wait for the pipeline to complete; however, a quick write up of all the changes you made in the context of the merge request will be generated. This functionality also exists in the content of an issue. Click on **Insert** to apply the message.

* [ ] **Step 6: Automatically Generated Test Cases** 
* ![generate_tests.svg](./generate_tests.svg)
  * Now that we have added code and have seen the security results we also want to add some testing to make sure it stays secure.
  * Use the **Code > Repository ** menu to view the contents of the repository, then click on the **notes>calc.py** file to view its contents.
  * Use the **Edit** dropdown menu and select **Open in Web IDE**
  * Highlight the class contents
  * Next right click and use the **GitLab Duo Chat > Generate Tests** menu, at which point a pop up on the left hand side will appear to give you a number of suggestions of test cases you could add to your project for unit testing.
  
  * This is the end of the hands on portion for this lab.

> Plan stage AI features typically have a start up time of 24 hours to train themselves, so if you want to see them in action make sure you start those tasks today

> [Docs for GitLab Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)
